import { ApiProperty } from '@nestjs/swagger';
import {
  IsBoolean,
  IsDate,
  IsNumber,
  IsOptional,
  IsString,
  MaxLength,
} from 'class-validator';

export class EditDto {
  @IsString()
  @ApiProperty({ required: true })
  @IsOptional()
  customerId: string;

  @IsNumber()
  @ApiProperty({ required: true })
  @IsOptional()
  subcriberId: number;

  @IsString()
  @ApiProperty({ required: true })
  @IsOptional()
  subcriberCode: string;

  @IsString()
  @ApiProperty({ required: true })
  @IsOptional()
  domainStatus: string;

  @IsString()
  @ApiProperty({ required: true })
  @IsOptional()
  domain: string;

  @IsString()
  @ApiProperty({ required: true })
  @IsOptional()
  startDate: Date;

  @IsString()
  @ApiProperty({ required: true })
  @IsOptional()
  endDate: Date;

  @IsString()
  @IsOptional()
  @ApiProperty({ required: false })
  @MaxLength(100)
  name: string;

  @IsString()
  @IsOptional()
  @ApiProperty({ required: false })
  note: string;

  @IsString()
  @IsOptional()
  @ApiProperty({ required: false, maxLength: 3000 })
  @MaxLength(3000)
  description: string;

  @IsBoolean()
  @IsOptional()
  @ApiProperty({ required: false })
  isActive: boolean;

  @IsString()
  @IsOptional()
  @ApiProperty({ required: false })
  updateBy: string;

  @IsOptional()
  @IsDate()
  @ApiProperty({ required: false })
  updateAt: Date;

  @IsOptional()
  @IsNumber()
  @ApiProperty({ required: false })
  updateAtTimestamp: number;

  @IsOptional()
  @IsDate()
  @ApiProperty({ required: false })
  createAt: Date;

  @IsOptional()
  @IsNumber()
  @ApiProperty({ required: false })
  createAtTimestamp: number;
}
