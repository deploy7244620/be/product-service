import { IBaseEntity } from 'common/interface/repository/interface.base.entities';
import { Column, Entity, ObjectIdColumn } from 'typeorm';

export interface IEntity extends IBaseEntity {
  name: string;
}
@Entity({ name: 'restaurants-reviews' })
export class EntityMongodb implements IEntity {
  @Column({ nullable: true })
  note: string;

  @Column({ nullable: true })
  description: string;

  @Column({ nullable: true })
  status: boolean;

  @Column({ nullable: true })
  isActive: boolean;

  @Column({ nullable: true })
  updateAt: Date;

  @Column({ nullable: true })
  updateAtTimestamp: number;

  @Column({ nullable: true })
  updateBy: string;

  @Column({ nullable: true })
  createBy: string;

  @Column({ nullable: true })
  createAtTimestamp: number;

  @ObjectIdColumn()
  _id: object;

  @Column({ nullable: true })
  createAt: Date;

  @Column({ nullable: true })
  name: string;
}
