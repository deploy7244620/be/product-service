import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Post,
  Put,
  Query,
  UsePipes,
  ValidationPipe,
} from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { CreateDto } from './dto/create.dto';
import { EditDto } from './dto/edit.dto';
import { GetListDto } from './dto/get-list.dto';
import { Service } from './service';
const rootFolder = __dirname.split('/').pop();

@Controller(rootFolder.toLocaleLowerCase())
@ApiTags(rootFolder)
export class ModuleController {
  constructor(private _service: Service) {}

  @Get('/search')
  async search(@Query() input) {
    return await this._service.search(input);
  }

  @Post()
  @UsePipes(new ValidationPipe({ whitelist: true }))
  async add(@Body() body: CreateDto) {
    return await this._service.add(body);
  }

  @Delete('/:id')
  async delete(@Param('id') p_id) {
    return await this._service.delete(p_id);
  }

  @Get()
  async getList(@Query() input: GetListDto) {
    const data = await this._service.select(input);
    // fill data id
    if (data.count > 0) {
      const promises = data.data.map((item) => {
        return Promise.all([
          this._service
            .getRestaurantsById(item['restaurantId'])
            .then((data) => {
              // Gán dữ liệu lấy được vào thuộc tính 'data' của mỗi item
              item['restaurant'] = data || {};
              return item;
            }),
          this._service.getById(item['parentId']).then((data) => {
            // Gán dữ liệu lấy được vào thuộc tính 'data' của mỗi item
            item['parent'] = data.data[0] || {};
            return item;
          }),
        ]).then(() => {
          return item;
        });
      });

      await Promise.all(promises)
        .then((results) => {
          return results;
        })
        .catch((error) => {
          console.error('Error:', error);
        });
    }
    return data;
  }

  @Get('/:id')
  async get(@Param('id') p_id) {
    const data = await this._service.select({ id: p_id });
    if (data.count > 0) {
      const promises = data.data.map((item) => {
        return Promise.all([
          this._service
            .getRestaurantsById(item['restaurantId'])
            .then((data) => {
              // Gán dữ liệu lấy được vào thuộc tính 'data' của mỗi item
              item['restaurant'] = data || {};
              return item;
            }),
          this._service.getById(item['parentId']).then((data) => {
            // Gán dữ liệu lấy được vào thuộc tính 'data' của mỗi item
            item['parent'] = data.data[0] || {};
            return item;
          }),
        ]).then(() => {
          return item;
        });
      });

      await Promise.all(promises)
        .then((results) => {
          return results;
        })
        .catch((error) => {
          console.error('Error:', error);
        });
    }
    return data;
  }

  @Put('/:id')
  @UsePipes(new ValidationPipe({ whitelist: true }))
  async edit(@Param('id') p_id, @Body() body: EditDto) {
    return await this._service.update(p_id, body);
  }
}
