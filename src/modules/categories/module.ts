import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { EntityMongodb } from './entity';
import { RepositoryMongodb } from './repository/repository.mongodb';
import { ModuleController } from './controller';
import { Service } from './service';
import { RestaurantsModule } from '../restaurants/module';

@Module({
  imports: [
    TypeOrmModule.forFeature([EntityMongodb], 'mongodb'),
    RestaurantsModule,
  ],
  controllers: [ModuleController],
  providers: [
    Service,
    {
      provide: 'repoMongodb',
      useClass: RepositoryMongodb,
    },
  ],
  exports: [Service],
})
export class CategoriesModule {}
