import { Inject, Injectable } from '@nestjs/common';
import { IRepository } from './repository/interface.repository';
import { IEntity } from './entity';
import { AService } from 'common/interface/service/abstract.service';
import { RepositoryProxy } from './repository/repository.proxy';
const rootFolder = __dirname.split('/').pop();
import * as ServiceRestaurants from '../restaurants/service';

@Injectable()
export class Service extends AService {
  constructor(
    @Inject('repoMongodb')
    private repositoryPostgres: IRepository<IEntity>,
    private serviceRestaurants: ServiceRestaurants.Service,
  ) {
    super(new RepositoryProxy<IEntity>(rootFolder, repositoryPostgres));
  }

  async getRestaurantsById(accountDomainId) {
    const data = await this.serviceRestaurants.select({
      id: accountDomainId,
    });
    return data.data[0];
  }
}
