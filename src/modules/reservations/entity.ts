import { IBaseEntity } from 'common/interface/repository/interface.base.entities';
import { Column, Entity, ObjectIdColumn } from 'typeorm';

export interface IEntity extends IBaseEntity {
  name: string;
  restaurantId: object;
  tableIds: object;
  timeReservation: Date;
  numberOfPeople: number;
  guestName: string;
  guestPhoneNumber: string;
  guestEmail: string;
  userId: string;
}
@Entity({ name: 'reservations' })
export class EntityMongodb implements IEntity {
  @Column({ nullable: true })
  tableIds: object;

  @Column({ nullable: true })
  note: string;

  @Column({ nullable: true })
  description: string;

  @Column({ nullable: true })
  status: boolean;

  @Column({ nullable: true })
  isActive: boolean;

  @Column({ nullable: true })
  updateAt: Date;

  @Column({ nullable: true })
  updateAtTimestamp: number;

  @Column({ nullable: true })
  updateBy: string;

  @Column({ nullable: true })
  createBy: string;

  @Column({ nullable: true })
  createAtTimestamp: number;

  @ObjectIdColumn()
  _id: object;

  @Column({ nullable: true })
  createAt: Date;

  @Column({ nullable: true })
  name: string;

  @Column({ nullable: true })
  restaurantId: object;

  // @Column({ nullable: true })
  // tableId: object;

  @Column({ nullable: true })
  timeReservation: Date;

  @Column({ nullable: true })
  numberOfPeople: number;

  @Column({ nullable: true })
  guestName: string;

  @Column({ nullable: true })
  guestPhoneNumber: string;

  @Column({ nullable: true })
  guestEmail: string;

  @Column({ nullable: true })
  userId: string;
}
