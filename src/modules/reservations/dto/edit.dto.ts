import { ApiProperty } from '@nestjs/swagger';
import { IsDate, IsNumber, IsOptional, IsString } from 'class-validator';

export class EditDto {
  @IsOptional()
  @IsString()
  @ApiProperty({ nullable: true })
  note: string;

  @IsOptional()
  @IsString()
  @ApiProperty({ nullable: true })
  description: string;

  @IsOptional()
  @ApiProperty({ nullable: true })
  status: boolean;

  @IsOptional()
  @ApiProperty({ nullable: true })
  isActive: boolean;

  @IsOptional()
  @ApiProperty({ nullable: true })
  updateAt: Date;

  @IsOptional()
  @ApiProperty({ nullable: true })
  updateAtTimestamp: number;

  @IsOptional()
  @ApiProperty({ nullable: true })
  updateBy: string;

  @IsOptional()
  @ApiProperty({ nullable: true })
  createBy: string;

  @IsOptional()
  @ApiProperty({ nullable: true })
  createAtTimestamp: number;

  @IsOptional()
  @ApiProperty({ nullable: true })
  createAt: Date;

  @IsOptional()
  @IsString()
  @ApiProperty({ nullable: true })
  name: string;

  @IsOptional()
  @ApiProperty({ nullable: true })
  restaurantId: object;

  @IsOptional()
  @ApiProperty({ nullable: true })
  tableId: object;

  @IsOptional()
  @IsDate()
  @ApiProperty({ nullable: true })
  timeReservation: Date;

  @IsOptional()
  @IsNumber()
  @ApiProperty({ nullable: true })
  numberOfPeople: number;

  @IsOptional()
  @IsString()
  @ApiProperty({ nullable: true })
  guestName: string;

  @IsOptional()
  @IsString()
  @ApiProperty({ nullable: true })
  guestPhoneNumber: string;

  @IsOptional()
  @IsString()
  @ApiProperty({ nullable: true })
  guestEmail: string;

  @IsOptional()
  @ApiProperty({ nullable: true })
  userId: string;
}
