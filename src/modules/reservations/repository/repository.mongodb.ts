import { Injectable } from '@nestjs/common';
import { DataSource } from 'typeorm';
import { InjectConnection } from '@nestjs/typeorm';
import { IEntity, EntityMongodb } from '../entity';
import { AbstractRepositoryMongodb } from 'common/interface/repository/abstract.repository.mongodb';
import { IRepository } from './interface.repository';

@Injectable()
export class RepositoryMongodb
  extends AbstractRepositoryMongodb<IEntity>
  implements IRepository<IEntity>
{
  constructor(@InjectConnection('mongodb') private _dataSource: DataSource) {
    // sua lai TaskEntity thanh Postgres
    super(_dataSource.getRepository(EntityMongodb));
  }
}
