import { IBaseRepository } from 'common/interface/repository/interface.repository';

export interface IRepository<T> extends IBaseRepository<T> {}
