import { HttpException, HttpStatus, Inject, Injectable } from '@nestjs/common';
import { IRepository } from './repository/interface.repository';
import { IEntity } from './entity';
import { AService } from 'common/interface/service/abstract.service';
import { RepositoryProxy } from './repository/repository.proxy';
// import { RepositoryElasticSearch } from './repository/repository.elasticsearch';
const rootFolder = __dirname.split('/').pop();
import * as ServiceTables from '../tables/service';

@Injectable()
export class Service extends AService {
  constructor(
    @Inject('repoMongodb')
    private repository: IRepository<IEntity>,
    private serviceTables: ServiceTables.Service,
  ) {
    super(new RepositoryProxy<IEntity>(rootFolder, repository));
  }

  async selectCustom(input: any) {
    try {
      const data = await this.select(input);
      if (data.count > 0) {
        const promises = data.data.map((item) => {
          return Promise.all([
            this.getTablesByArrayId(item['tableIds']).then((data) => {
              item['tables'] = data || [];
              return item;
            }),
          ]).then(() => {
            return item;
          });
        });

        await Promise.all(promises)
          .then((results) => {
            return results;
          })
          .catch((error) => {
            console.error('Error:', error);
          });
      }
      return data;
    } catch (error) {
      throw new HttpException(error, HttpStatus.BAD_GATEWAY);
    }
  }
  async getTablesByArrayId(arrayId) {
    const promises = arrayId.map((item) => {
      return this.serviceTables.select({ id: item }).then((data) => {
        return data.data[0] || {};
      });
    });
    const data = await Promise.all(promises)
      .then((results) => {
        return results;
      })
      .catch((error) => {
        console.error('Error:', error);
      });
    return data;
  }
}
