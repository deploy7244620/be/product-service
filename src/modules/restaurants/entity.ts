import { IBaseEntity } from 'common/interface/repository/interface.base.entities';
import { Column, Entity, ObjectIdColumn } from 'typeorm';

export interface IEntity extends IBaseEntity {
  name: string; // tên cửa hàng
  address: string;
  Phone: string;
  owner: string; // chủ cửa hàng
  CCCD: string; // căn cước công dân chủ cửa hàng
  capacity: number; // sức chứa tối đa của nhà hàng
  cuisineType: string; // loại quán ăn (bún bò, bún đậu mắm tôm, cơm tấm ...)
  facilities: string; // các tiện lợi của quán ăn (wifi, đỗ xe, bàn ngoài trời ....)
  locationCoordinates: object; // tọa độ của nhà hàng
  imagePaths: object; // danh sách ảnh về cửa hàng
  socialMediaLinks: object; // link mạng xã hội
  statusRestaurants: string; // trạng thái quán: OPEN, CLOSE, STOP_WORKING
  openingHours: object; // thời gian hoạt động của quán
}
@Entity({ name: 'restaurants' })
export class EntityMongodb implements IEntity {
  @Column({ nullable: true })
  address: string;

  @Column({ nullable: true })
  Phone: string;

  @Column({ nullable: true })
  owner: string;

  @Column({ nullable: true })
  CCCD: string;

  @Column({ nullable: true })
  capacity: number;

  @Column({ nullable: true })
  cuisineType: string;

  @Column({ nullable: true })
  facilities: string;

  @Column({ nullable: true })
  locationCoordinates: object;

  @Column({ nullable: true })
  imagePaths: object;

  @Column({ nullable: true })
  socialMediaLinks: object;

  @Column({ nullable: true })
  statusRestaurants: string;

  @Column({ nullable: true })
  openingHours: object;

  @Column({ nullable: true })
  note: string;

  @Column({ nullable: true })
  description: string;

  @Column({ nullable: true })
  status: boolean;

  @Column({ nullable: true })
  isActive: boolean;

  @Column({ nullable: true })
  updateAt: Date;

  @Column({ nullable: true })
  updateAtTimestamp: number;

  @Column({ nullable: true })
  updateBy: string;

  @Column({ nullable: true })
  createBy: string;

  @Column({ nullable: true })
  createAtTimestamp: number;

  @ObjectIdColumn()
  _id: object;

  @Column({ nullable: true })
  createAt: Date;

  @Column({ nullable: true })
  name: string;
}
