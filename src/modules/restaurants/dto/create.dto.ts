import { ApiProperty } from '@nestjs/swagger';
import { IsNumber, IsObject, IsOptional, IsString } from 'class-validator';

export class CreateDto {
  @IsOptional()
  @IsString()
  @ApiProperty({ nullable: true })
  address: string;

  @IsOptional()
  @IsString()
  @ApiProperty({ nullable: true })
  Phone: string;

  @IsOptional()
  @IsString()
  @ApiProperty({ nullable: true })
  owner: string;

  @IsOptional()
  @IsString()
  @ApiProperty({ nullable: true })
  CCCD: string;

  @IsOptional()
  @IsNumber()
  @ApiProperty({ nullable: true })
  capacity: number;

  @IsOptional()
  @IsString()
  @ApiProperty({ nullable: true })
  cuisineType: string;

  @IsOptional()
  @ApiProperty({ nullable: true })
  facilities: object;

  @IsOptional()
  @IsObject()
  @ApiProperty({ nullable: true })
  locationCoordinates: object;

  @IsOptional()
  @ApiProperty({ nullable: true })
  imagePaths: object;

  @IsOptional()
  @IsObject()
  @ApiProperty({ nullable: true })
  socialMediaLinks: object;

  @IsOptional()
  @IsString()
  @ApiProperty({ nullable: true })
  statusRestaurants: string;

  @IsOptional()
  @IsObject()
  @ApiProperty({ nullable: true })
  openingHours: object;

  @IsOptional()
  @IsString()
  @ApiProperty({ nullable: true })
  name: string;

  @IsOptional()
  @IsString()
  @ApiProperty({ nullable: true })
  note: string;

  @IsOptional()
  @IsString()
  @ApiProperty({ nullable: true })
  description: string;

  @IsOptional()
  @ApiProperty({ nullable: true })
  status: boolean;

  @IsOptional()
  @ApiProperty({ nullable: true })
  isActive: boolean;

  @IsOptional()
  @ApiProperty({ nullable: true })
  updateAt: Date;

  @IsOptional()
  @ApiProperty({ nullable: true })
  updateAtTimestamp: number;

  @IsOptional()
  @ApiProperty({ nullable: true })
  updateBy: string;

  @IsOptional()
  @ApiProperty({ nullable: true })
  createAt: Date;

  @IsOptional()
  @ApiProperty({ nullable: true })
  createAtTimestamp: number;

  @IsOptional()
  @ApiProperty({ nullable: true })
  createBy: string;
}
