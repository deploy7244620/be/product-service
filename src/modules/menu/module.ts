import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { EntityMongodb } from './entity';
import { RepositoryMongodb } from './repository/repository.mongodb';
import { ModuleController } from './controller';
import { Service } from './service';
import { RestaurantsModule } from '../restaurants/module';
import { CategoriesModule } from '../categories/module';

@Module({
  imports: [
    TypeOrmModule.forFeature([EntityMongodb], 'mongodb'),
    RestaurantsModule,
    CategoriesModule,
  ],

  controllers: [ModuleController],
  providers: [
    Service,
    {
      provide: 'repoMongodb',
      useClass: RepositoryMongodb,
    },
  ],
  exports: [Service],
})
export class MenuModule {}
