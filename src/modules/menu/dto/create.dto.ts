import { ApiProperty } from '@nestjs/swagger';
import {
  IsArray,
  IsNotEmpty,
  IsNumber,
  IsOptional,
  IsString,
} from 'class-validator';

export class CreateDto {
  @IsNumber()
  @ApiProperty({ nullable: true })
  price: number;

  @IsArray()
  @ApiProperty({ nullable: true })
  addOn: object;

  @IsString()
  @ApiProperty({ nullable: true })
  categoriesId: object; // array

  @IsNumber()
  @ApiProperty({ nullable: true })
  displayOrder: number;

  @IsString()
  @IsNotEmpty()
  @ApiProperty({ nullable: true })
  restaurantId: object;

  @IsOptional()
  @ApiProperty({ nullable: true })
  imageUrl: object;

  @IsOptional()
  @ApiProperty({ nullable: true })
  timeOfDay: object; // sáng, trưa, tối

  @IsOptional()
  @IsString()
  @ApiProperty({ nullable: true })
  name: string;

  @IsOptional()
  @IsString()
  @ApiProperty({ nullable: true })
  note: string;

  @IsOptional()
  @IsString()
  @ApiProperty({ nullable: true })
  description: string;

  @IsOptional()
  @ApiProperty({ nullable: true })
  status: boolean;

  @IsOptional()
  @ApiProperty({ nullable: true })
  isActive: boolean;

  @IsOptional()
  @ApiProperty({ nullable: true })
  updateAt: Date;

  @IsOptional()
  @ApiProperty({ nullable: true })
  updateAtTimestamp: number;

  @IsOptional()
  @ApiProperty({ nullable: true })
  updateBy: string;

  @IsOptional()
  @ApiProperty({ nullable: true })
  createAt: Date;

  @IsOptional()
  @ApiProperty({ nullable: true })
  createAtTimestamp: number;

  @IsOptional()
  @ApiProperty({ nullable: true })
  createBy: string;
}
