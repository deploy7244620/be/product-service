import { ApiProperty } from '@nestjs/swagger';
import { Transform, Type } from 'class-transformer';
import {
  IsAlphanumeric,
  IsArray,
  IsBoolean,
  IsDate,
  isNumber,
  IsNumber,
  isObject,
  IsObject,
  IsOptional,
  IsString,
  IsUUID,
  MaxLength,
  ValidateNested,
} from 'class-validator';
import { IEntity } from '../entity';

export class EditDto {
  @IsNumber()
  @IsOptional()
  @ApiProperty({ nullable: true })
  price: number;

  @IsArray()
  @IsOptional()
  @ApiProperty({ nullable: true })
  addOn: object;

  @IsArray()
  @IsOptional()
  @ApiProperty({ nullable: true })
  categoriesIds: object; // array

  @IsNumber()
  @IsOptional()
  @ApiProperty({ nullable: true })
  displayOrder: number;

  @IsString()
  @IsOptional()
  @ApiProperty({ nullable: true })
  restaurantId: object;

  @IsOptional()
  @ApiProperty({ nullable: true })
  imageUrl: object;

  @IsOptional()
  @ApiProperty({ nullable: true })
  timeOfDay: object; // sáng, trưa, tối

  @IsOptional()
  @IsString()
  @ApiProperty({ nullable: true })
  name: string;

  @IsOptional()
  @IsString()
  @ApiProperty({ nullable: true })
  note: string;

  @IsOptional()
  @IsString()
  @ApiProperty({ nullable: true })
  description: string;

  @IsOptional()
  @ApiProperty({ nullable: true })
  status: boolean;

  @IsOptional()
  @ApiProperty({ nullable: true })
  isActive: boolean;

  @IsOptional()
  @ApiProperty({ nullable: true })
  updateAt: Date;

  @IsOptional()
  @ApiProperty({ nullable: true })
  updateAtTimestamp: number;

  @IsOptional()
  @ApiProperty({ nullable: true })
  updateBy: string;

  @IsOptional()
  @ApiProperty({ nullable: true })
  createAt: Date;

  @IsOptional()
  @ApiProperty({ nullable: true })
  createAtTimestamp: number;

  @IsOptional()
  @ApiProperty({ nullable: true })
  createBy: string;
}
