import { IBaseEntity } from 'common/interface/repository/interface.base.entities';
import { Column, Entity, Index, ObjectIdColumn } from 'typeorm';

export interface IEntity extends IBaseEntity {
  price: number;
  name: string;
  categoriesId: object;
  displayOrder: number;
  restaurantId: object;
  imageUrl: object;
  timeOfDay: object; // sáng, trưa, tối
  addOn: object; // tùy chọn thêm
}
@Entity({ name: 'menu' })
@Index(['restaurantId'])
export class EntityMongodb implements IEntity {
  @Column({ nullable: true })
  price: number;

  @Column({ nullable: true })
  addOn: object;

  @Column({ nullable: true })
  name: string;

  @Column({ nullable: true })
  categoriesId: object;

  @Column({ nullable: true })
  displayOrder: number;

  @Column({ nullable: true })
  restaurantId: object;

  @Column({ nullable: true })
  imageUrl: object;

  @Column({ nullable: true })
  timeOfDay: object;

  @Column({ nullable: true })
  note: string;

  @Column({ nullable: true })
  description: string;

  @Column({ nullable: true })
  status: boolean;

  @Column({ nullable: true })
  isActive: boolean;

  @Column({ nullable: true })
  updateAt: Date;

  @Column({ nullable: true })
  updateAtTimestamp: number;

  @Column({ nullable: true })
  updateBy: string;

  @Column({ nullable: true })
  createBy: string;

  @Column({ nullable: true })
  createAtTimestamp: number;

  @ObjectIdColumn()
  _id: object;

  @Column({ nullable: true })
  createAt: Date;
}
