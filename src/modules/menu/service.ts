import { Inject, Injectable } from '@nestjs/common';
import { IRepository } from './repository/interface.repository';
import { IEntity } from './entity';
import { AService } from 'common/interface/service/abstract.service';
import { RepositoryProxy } from './repository/repository.proxy';
const rootFolder = __dirname.split('/').pop();
import * as ServiceRestaurants from '../restaurants/service';
import * as ServiceCategories from '../categories/service';

@Injectable()
export class Service extends AService {
  constructor(
    @Inject('repoMongodb')
    private repositoryPostgres: IRepository<IEntity>,
    private serviceRestaurants: ServiceRestaurants.Service,
    private serviceCategories: ServiceCategories.Service,
  ) {
    super(new RepositoryProxy<IEntity>(rootFolder, repositoryPostgres));
  }
  async getRestaurantsById(id) {
    const data = await this.serviceRestaurants.select({
      id: id,
    });
    return data.data[0];
  }
  async getCategoriesById(id) {
    const data = await this.serviceCategories.select({
      id: id,
    });
    return data.data[0];
  }
}
