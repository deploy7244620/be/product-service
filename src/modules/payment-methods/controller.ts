import {
  Body,
  Controller,
  Get,
  Param,
  Post,
  Query,
  UsePipes,
  ValidationPipe,
} from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { CreateDto } from './dto/create.dto';
import { GetListDto } from './dto/get-list.dto';
import { Service } from './service';
const rootFolder = __dirname.split('/').pop();

@Controller(rootFolder.toLocaleLowerCase())
@ApiTags(rootFolder)
export class ModuleController {
  constructor(private _service: Service) {}

  @Get('/search')
  async search(@Query() input) {
    return await this._service.search(input);
  }

  @Post()
  @UsePipes(new ValidationPipe({ whitelist: true }))
  async add(@Body() body: CreateDto) {
    return await this._service.add(body);
  }

  // @Delete('/:id')
  // async delete(@Param('id') p_id) {
  //   return await this._service.delete(p_id);
  // }

  @Get()
  async getList(@Query() input: GetListDto) {
    return await this._service.select(input);
  }

  @Get('/:id')
  async get(@Param('id') p_id) {
    return await this._service.select({ id: p_id });
  }

  // @Put('/:id')
  // async edit(@Param('id') p_id, @Body() body: EditDto) {
  //   return await this._service.update(p_id, body);
  // }
}
