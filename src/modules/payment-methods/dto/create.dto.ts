import { ApiProperty } from '@nestjs/swagger';
import { IsDate, IsOptional } from 'class-validator';

export class CreateDto {
  @IsOptional()
  @IsDate()
  @ApiProperty({ required: false })
  createAt: Date;
}
