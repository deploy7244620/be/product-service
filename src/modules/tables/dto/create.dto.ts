import { ApiProperty } from '@nestjs/swagger';
import {
  IsBoolean,
  IsNotEmpty,
  IsNumber,
  IsOptional,
  IsString,
} from 'class-validator';

export class CreateDto {
  @IsNotEmpty()
  @ApiProperty({ nullable: true })
  restaurantId: object;

  @IsNumber()
  @ApiProperty({ nullable: true })
  capacity: number;

  @IsOptional()
  @IsString()
  @ApiProperty({ nullable: true })
  location: string;

  @IsOptional()
  @IsString()
  @ApiProperty({ nullable: true })
  type: string;

  @IsOptional()
  @IsString()
  @ApiProperty({ nullable: true })
  name: string;

  @IsOptional()
  @IsString()
  @ApiProperty({ nullable: true })
  note: string;

  @IsOptional()
  @IsString()
  @ApiProperty({ nullable: true })
  description: string;

  @IsOptional()
  @ApiProperty({ nullable: true })
  status: boolean;

  @IsBoolean()
  @ApiProperty({ nullable: true })
  isActive: boolean;

  @IsOptional()
  @ApiProperty({ nullable: true })
  updateAt: Date;

  @IsOptional()
  @ApiProperty({ nullable: true })
  updateAtTimestamp: number;

  @IsOptional()
  @ApiProperty({ nullable: true })
  updateBy: string;

  @IsOptional()
  @ApiProperty({ nullable: true })
  createAt: Date;

  @IsOptional()
  @ApiProperty({ nullable: true })
  createAtTimestamp: number;

  @IsOptional()
  @ApiProperty({ nullable: true })
  createBy: string;
}
