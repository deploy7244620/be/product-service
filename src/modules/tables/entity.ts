import { IBaseEntity } from 'common/interface/repository/interface.base.entities';
import { Column, Entity, ObjectIdColumn } from 'typeorm';

export interface IEntity extends IBaseEntity {
  name: string;
  restaurantId: object; // liên kết với bảng Restaurants
  capacity: number; //Sức chứa của bàn
  location: string; //Vị trí của bàn trong nhà hàng, "gần cửa sổ", "trong khu vực VIP", hoặc "ngoài trời".
  type: string; //Loại bàn, ví dụ như bàn tiêu chuẩn, bàn bar, hoặc bàn dành cho sự kiện đặc biệt.
}
@Entity({ name: 'tables' })
export class EntityMongodb implements IEntity {
  @Column({ nullable: true })
  restaurantId: object;

  @Column({ nullable: true })
  capacity: number;

  @Column({ nullable: true })
  location: string;

  @Column({ nullable: true })
  type: string;

  @Column({ nullable: true })
  note: string;

  @Column({ nullable: true })
  description: string;

  @Column({ nullable: true })
  status: boolean;

  @Column({ nullable: true })
  isActive: boolean;

  @Column({ nullable: true })
  updateAt: Date;

  @Column({ nullable: true })
  updateAtTimestamp: number;

  @Column({ nullable: true })
  updateBy: string;

  @Column({ nullable: true })
  createBy: string;

  @Column({ nullable: true })
  createAtTimestamp: number;

  @ObjectIdColumn()
  _id: object;

  @Column({ nullable: true })
  createAt: Date;

  @Column({ nullable: true })
  name: string;
}
