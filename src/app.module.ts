import {
  MiddlewareConsumer,
  Module,
  NestModule,
  RequestMethod,
} from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { DiscoveryDecoratorModule } from 'common/modules/discovery-decorator/discovery-decorator';
import { HealthModule } from 'common/modules/health/module';
import { APP_INTERCEPTOR } from '@nestjs/core';
import { AddDefaultInterceptor } from 'common/interceptor/interceptor.add-default';
import { MenuModule } from './modules/menu/module';
import { CategoriesModule } from './modules/categories/module';
import { RestaurantsModule } from './modules/restaurants/module';
import { TableModule } from './modules/tables/module';
import { ReservationsModule } from './modules/reservations/module';

@Module({
  imports: [
    MenuModule,
    CategoriesModule,
    RestaurantsModule,
    TableModule,
    ReservationsModule,
    HealthModule,
    DiscoveryDecoratorModule,
    TypeOrmModule.forRoot({
      type: 'mongodb',
      name: 'mongodb',
      url: process.env.MONGODB_DATABASE,
      entities: [],
      synchronize: true,
      autoLoadEntities: true,
    }),
  ],
  controllers: [],
  providers: [
    {
      provide: APP_INTERCEPTOR,
      useClass: AddDefaultInterceptor,
    },
  ],
})
export class AppModule implements NestModule {
  configure(consumer: MiddlewareConsumer) {
    consumer
      .apply()
      .exclude({ path: '/health', method: RequestMethod.ALL })
      .forRoutes('*');
  }
}
