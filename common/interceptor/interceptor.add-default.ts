import {
  CACHE_MANAGER,
  CallHandler,
  ExecutionContext,
  Inject,
  Injectable,
  NestInterceptor,
} from '@nestjs/common';
import { map, Observable, tap } from 'rxjs';
import { v4 as uuidv4 } from 'uuid';
import { Cache } from 'cache-manager';
import { InjectRedis, Redis } from '@nestjs-modules/ioredis';
import * as pack from '../../package.json';
let ttl = Number(process.env.REDIS_DEFAULT_TTL) || 0;
export const AddDefault = (input?, method?) => {
  switch (method) {
    case 'POST':
      {
        input.updateBy = input.createBy || '';
        input.updateAt = new Date();
        input.updateAtTimestamp = new Date().getTime();
        input.createAt = new Date();
        input.createAtTimestamp = new Date().getTime();
        input.status = true;
        input.isActive = true;
      }
      break;
    case 'add':
      {
        input.updateBy = input.createBy || '';
        input.updateAt = new Date();
        input.updateAtTimestamp = new Date().getTime();
        input.createAt = new Date();
        input.createAtTimestamp = new Date().getTime();
        input.status = true;
        input.isActive = true;
      }
      break;
    case 'PUT':
      {
        input.updateAt = new Date();
        input.updateAtTimestamp = new Date().getTime();
      }
      break;
    case 'edit':
      {
        input.updateAt = new Date();
        input.updateAtTimestamp = new Date().getTime();
      }
      break;
    default:
      break;
  }
  return input;
};
@Injectable()
export class AddDefaultInterceptor implements NestInterceptor {
  // constructor(
  //   @InjectRedis('redisW') private readonly redisW: Redis,
  //   @InjectRedis('redisR') private readonly redisR: Redis,
  // ) {}

  async intercept(context: ExecutionContext, next: CallHandler): Promise<any> {
    // eslint-disable-next-line prefer-const
    let { method, body, originalUrl, query } = context
      .switchToHttp()
      .getRequest();
    const keyRedis = `${pack.name}:${
      process.env.NODE_ENV || 'development'
    }:${originalUrl}`;
    body = AddDefault(body, method);

    return next.handle().pipe(
      map(async (value) => {
        return returnSuccess(value);
      }),
    );
  }
}

// function returnSuccess(params: type) {}

// function returnError(params: type) {}

export const returnSuccess = (result: any, cache = false, ttl = 0) => {
  return {
    result: result,
    cache: cache,
    ttl: ttl,
    time: new Date(),
  };
};
