import { DiscoveryModule, DiscoveryService } from '@golevelup/nestjs-discovery';
import { Injectable, Module, OnModuleInit } from '@nestjs/common';
import {
  META_DATA_SUBSCRIBE,
  META_DATA_SUBSCRIBE_CUSTOM,
  META_DATA_SUBSCRIBE_CUSTOM_PROVIDER,
  META_DATA_SUBSCRIBE_EXPIRE_TIME,
} from 'common/libs/constants';
import { MessageBus } from 'common/libs/message-bus';
import * as pack from '../../../package.json';
const NAME_SERVICE = pack.name || 'project_base';
const NODE_ENV = process.env.NODE_ENV || 'DEV';
const messageBus = new MessageBus();
import { Logger } from '@nestjs/common';

@Module({
  imports: [DiscoveryModule],
})
@Injectable()
export class DiscoveryDecoratorModule implements OnModuleInit {
  constructor(private readonly discover: DiscoveryService) {}

  public async onModuleInit() {
    this.execQueueRabbit(
      await this.discover.methodsAndControllerMethodsWithMetaAtKey<string>(
        META_DATA_SUBSCRIBE,
      ),
    );
    this.execQueueRabbitExpireTime(
      await this.discover.methodsAndControllerMethodsWithMetaAtKey<string>(
        META_DATA_SUBSCRIBE_EXPIRE_TIME,
      ),
    );
    this.execQueueRabbitCustomProvider(
      await this.discover.providerMethodsWithMetaAtKey<string>(
        META_DATA_SUBSCRIBE_CUSTOM_PROVIDER,
      ),
    );

    this.execQueueRabbitCustom(
      await this.discover.methodsAndControllerMethodsWithMetaAtKey<string>(
        META_DATA_SUBSCRIBE_CUSTOM,
      ),
    );
  }

  async execQueueRabbitCustom(methodAndControllerSubscriber) {
    const logQueueInfo = [];
    for (const iterator of methodAndControllerSubscriber) {
      const nameInstance = iterator.discoveredMethod.parentClass.name;
      const nameFunc = iterator.discoveredMethod.methodName;
      const nameQueue = iterator.meta.nameQueue;
      const nameExchange = iterator.meta.nameExchange;
      const typeExchange = iterator.meta.typeExchange;
      const routingKey = iterator.meta.routingKey;
      const prefetch = iterator.meta.prefetch;
      const instance = await iterator.discoveredMethod.parentClass.instance;

      messageBus.runCustom(
        instance,
        nameFunc,
        nameQueue,
        nameExchange,
        typeExchange,
        routingKey,
        prefetch,
      );
      logQueueInfo.push({
        queue_name: nameQueue,
        controller_name: nameInstance,
        function_name: `${nameFunc}`,
        nameExchange,
        typeExchange,
        routingKey,
        prefetch,
      });
    }
    if (logQueueInfo.length > 0) {
      Logger.log('Queue methodAndController Custom');
      console.table(logQueueInfo);
    }
  }

  async execQueueRabbitCustomProvider(methodAndControllerSubscriber) {
    const logQueueInfo = [];
    for (const iterator of methodAndControllerSubscriber) {
      const nameInstance = iterator.discoveredMethod.parentClass.name;
      const nameFunc = iterator.discoveredMethod.methodName;
      const nameQueue = iterator.meta.nameQueue;
      const nameExchange = iterator.meta.nameExchange;
      const typeExchange = iterator.meta.typeExchange;
      const routingKey = iterator.meta.routingKey;
      const prefetch = iterator.meta.prefetch;
      const instance = await iterator.discoveredMethod.parentClass.instance;

      messageBus.runCustomProvider(
        instance,
        nameFunc,
        nameQueue,
        nameExchange,
        typeExchange,
        routingKey,
        prefetch,
      );
      logQueueInfo.push({
        queue_name: nameQueue,
        controller_name: nameInstance,
        function_name: `${nameFunc}`,
        nameExchange,
        typeExchange,
        routingKey,
        prefetch,
      });
    }
    if (logQueueInfo.length > 0) {
      Logger.log('Queue methodAndController Custom');
      console.table(logQueueInfo);
    }
  }

  async execQueueRabbit(methodAndControllerSubscriber) {
    const logQueueInfo = [];
    for (const iterator of methodAndControllerSubscriber) {
      const nameInstance = iterator.discoveredMethod.parentClass.name;
      const nameFunc = iterator.discoveredMethod.methodName;
      const nameQueue = iterator.meta.nameQueue;
      const instance = await iterator.discoveredMethod.parentClass.instance;
      const interceptor = iterator.meta.interceptor
        ? iterator.meta.interceptor
        : null;
      const queue =
        nameQueue || messageBus.buildNameQueue(nameFunc, nameInstance);
      messageBus.run(instance, nameFunc, queue, interceptor);
      logQueueInfo.push({
        queue_name: queue,
        controller_name: nameInstance,
        function_name: `${nameFunc}`,
      });
    }
    if (logQueueInfo.length > 0) {
      Logger.log('Queue methodAndController ');
      console.table(logQueueInfo);
    }
  }
  async execQueueRabbitExpireTime(methodAndControllerSubscriber) {
    const logQueueInfo = [];
    for (const iterator of methodAndControllerSubscriber) {
      const nameInstance = iterator.discoveredMethod.parentClass.name;
      const nameFunc = iterator.discoveredMethod.methodName;
      const nameQueue = iterator.meta.nameQueue;
      const instance = await iterator.discoveredMethod.parentClass.instance;
      const interceptor = iterator.meta.interceptor
        ? iterator.meta.interceptor
        : null;
      const queue =
        nameQueue || messageBus.buildNameQueue(nameFunc, nameInstance);
      messageBus.runExpireTTL(instance, nameFunc, queue, interceptor);
      logQueueInfo.push({
        queue_name: queue,
        controller_name: nameInstance,
        function_name: `${nameFunc}`,
      });
    }
    if (logQueueInfo.length > 0) {
      Logger.log('Queue methodAndController ');
      console.table(logQueueInfo);
    }
  }
}
