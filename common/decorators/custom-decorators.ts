import { SetMetadata } from '@nestjs/common';
import {
  META_DATA_SUBSCRIBE,
  META_DATA_SUBSCRIBE_CUSTOM,
  META_DATA_SUBSCRIBE_CUSTOM_PROVIDER,
  META_DATA_SUBSCRIBE_EXPIRE_TIME
} from 'common/libs/constants';
const REDIS_CONSTANTS = {
  KEYWORD: { name: 'KEYWORD', type: 'keyword' },
  ID: { name: 'ID', type: 'id' },
  NUMBER: { name: 'NUMBER', type: 'number' },
};
export const Subscribe = (nameQueue?, interceptor?) => {
  return (target: any, propertyKey: string, descriptor: PropertyDescriptor) => {
    SetMetadata(META_DATA_SUBSCRIBE, {
      nameQueue,
      interceptor,
    })(target, propertyKey, descriptor);
  };
};

export const SubscribeExpireTime = (nameQueue?, interceptor?) => {
  return (target: any, propertyKey: string, descriptor: PropertyDescriptor) => {
    SetMetadata(META_DATA_SUBSCRIBE_EXPIRE_TIME, {
      nameQueue,
      interceptor,
    })(target, propertyKey, descriptor);
  };
};
/**
 *  Khoi tao worker custom
 * @param {string} nameQueue
 * @param {string} nameExchange
 * @param {string} typeExchange direct, fanout, topic. default = direct
 * @param {string} routingKey
 * @param {number} prefetch
 * @param {number} pathPayload []
 * @returns
 */
export const SubscribeCustom = (
  nameQueue,
  nameExchange?,
  typeExchange = 'direct',
  routingKey?,
  prefetch = 1,
) => {
  return (target: any, propertyKey: string, descriptor: PropertyDescriptor) => {
    SetMetadata(META_DATA_SUBSCRIBE_CUSTOM, {
      nameQueue,
      nameExchange,
      typeExchange,
      routingKey,
      prefetch,
    })(target, propertyKey, descriptor);
  };
};

export const SubscribeCustomProvider = (
  nameQueue,
  nameExchange?,
  typeExchange = 'direct',
  routingKey?,
  prefetch = 1,
) => {
  return (target: any, propertyKey: string, descriptor: PropertyDescriptor) => {
    SetMetadata(META_DATA_SUBSCRIBE_CUSTOM_PROVIDER, {
      nameQueue,
      nameExchange,
      typeExchange,
      routingKey,
      prefetch,
    })(target, propertyKey, descriptor);
  };
};

export function Keyword(option?: any): PropertyDecorator {
  return function (target: any, propertyKey: string) {
    const metaDataValue =
      Reflect.getMetadata(REDIS_CONSTANTS.KEYWORD.name, target) || {};
    metaDataValue[propertyKey] = { type: REDIS_CONSTANTS.KEYWORD.type, option };
    Reflect.defineMetadata(REDIS_CONSTANTS.KEYWORD.name, metaDataValue, target);
  };
}

export function Id(option?: any): PropertyDecorator {
  return function (target: any, propertyKey: string) {
    const metaDataValue =
      Reflect.getMetadata(REDIS_CONSTANTS.ID.name, target) || {};
    metaDataValue[propertyKey] = { type: REDIS_CONSTANTS.ID.type, option };
    Reflect.defineMetadata(REDIS_CONSTANTS.ID.name, metaDataValue, target);
  };
}
export function Number(option?: any): PropertyDecorator {
  return function (target: any, propertyKey: string) {
    const metaDataValue =
      Reflect.getMetadata(REDIS_CONSTANTS.NUMBER.name, target) || {};
    metaDataValue[propertyKey] = { type: REDIS_CONSTANTS.NUMBER.type, option };
    Reflect.defineMetadata(REDIS_CONSTANTS.NUMBER.name, metaDataValue, target);
  };
}

export function RedisClassDecorator<T extends { new (...args: any[]): {} }>(
  target?: T,
): any {
  return class extends target {
    defineKeyRedis = [];
    constructor(...args: any[]) {
      super();
      const metaDataKeyword = Reflect.getMetadata(
        REDIS_CONSTANTS.KEYWORD.name,
        this,
      );
      for (const key in metaDataKeyword) {
        if (Object.prototype.hasOwnProperty.call(metaDataKeyword, key)) {
          const element = metaDataKeyword[key];
          if (element.option) {
            element.option.forEach((el) => {
              this.defineKeyRedis.push({
                nameField: key,
                configField: element,
                nameScore: el,
              });
            });
          } else {
            this.defineKeyRedis.push({
              nameField: key,
              configField: element,
            });
          }
        }
      }
      const metaDataId = Reflect.getMetadata(REDIS_CONSTANTS.ID.name, this);
      for (const key in metaDataId) {
        if (Object.prototype.hasOwnProperty.call(metaDataId, key)) {
          const element = metaDataId[key];
          this.defineKeyRedis.push({
            nameField: key,
            configField: element,
          });
        }
      }
      const metaDataNumber = Reflect.getMetadata(
        REDIS_CONSTANTS.NUMBER.name,
        this,
      );
      for (const key in metaDataNumber) {
        if (Object.prototype.hasOwnProperty.call(metaDataNumber, key)) {
          const element = metaDataNumber[key];
          this.defineKeyRedis.push({
            nameField: key,
            configField: element,
          });
        }
      }
    }
  };
  // return (target: any, propertyKey: string, descriptor: PropertyDescriptor) => {
  //   class haha extends target {}
  // };
}
