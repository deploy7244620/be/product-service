import { plainToClass } from 'class-transformer';

export function ReponseDto(reponseDto) {
  return function (
    target: Record<string, any>,
    _,
    descriptor: PropertyDescriptor,
  ) {
    const method = descriptor.value;
    descriptor.value = function (...args: Array<any>) {
      const myPromise = new Promise((resolve, reject) => {
        resolve(method.apply(this, args));
      }).then((value) => {
        return plainToClass(reponseDto, value, {
          exposeDefaultValues: true,
          enableCircularCheck: true,
        });
      });
      return myPromise;
    };
  };
}
