import { createDecipheriv } from 'crypto';

export class Crypto {
  private secretKey;
  private algorithm;
  constructor(
    _secretKey = '8jVY5zJiS*I&vh0fvHTh5c@g#eDfLyOW0vudT8UFM#IQ5LBfAr',
    _algorithm = 'aes-256-ctr',
  ) {
    this.secretKey = _secretKey;
    this.algorithm = _algorithm;
  }

  decrypt(iv, content) {
    const decipher = createDecipheriv(
      this.algorithm,
      this.secretKey,
      Buffer.from(iv, 'hex'),
    );

    const decrpyted = Buffer.concat([
      decipher.update(Buffer.from(content, 'hex')),
      decipher.final(),
    ]);

    return decrpyted.toString();
  }
}
