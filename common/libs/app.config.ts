const ROOT_API = process.env.ROOT_API || 'https://api-dev-rnd.cmctelecom.vn';
export const config = {
  'product-service': {
    config: {
      baseURL: `${ROOT_API}/api/v1/product-service`,
      timeout: 10000,
    },
    module: {
      'addon-attribute': {
        config: {
          baseURL: '/addon-attribute',
        },
        function: {
          add: {
            method: 'post',
            url: () => ``,
          },
          edit: {
            method: 'put',
            url: ({ id }) => `/${id}`,
          },
          getList: {
            method: 'get',
            url: () => ``,
          },
          delete: {
            method: 'delete',
            url: ({ id }) => `/${id}`,
          },
          get: {
            method: 'get',
            url: ({ id }) => `/${id}`,
          },
          search: {
            method: 'get',
            url: () => `/search`,
          },
        },
      },
      'package-addon': {
        config: {
          baseURL: '/package-addon',
        },
        function: {
          add: {
            method: 'post',
            url: () => ``,
          },
          edit: {
            method: 'put',
            url: ({ id }) => `/${id}`,
          },
          getList: {
            method: 'get',
            url: () => ``,
          },
          delete: {
            method: 'delete',
            url: ({ id }) => `/${id}`,
          },
          get: {
            method: 'get',
            url: ({ id }) => `/${id}`,
          },
          search: {
            method: 'get',
            url: () => `/search`,
          },
        },
      },
      'package-component': {
        config: {
          baseURL: '/package-component',
        },
        function: {
          add: {
            method: 'post',
            url: () => ``,
          },
          edit: {
            method: 'put',
            url: ({ id }) => `/${id}`,
          },
          getList: {
            method: 'get',
            url: () => ``,
          },
          delete: {
            method: 'delete',
            url: ({ id }) => `/${id}`,
          },
          get: {
            method: 'get',
            url: ({ id }) => `/${id}`,
          },
          search: {
            method: 'get',
            url: () => `/search`,
          },
        },
      },
      'package-fee': {
        config: {
          baseURL: '/package-fee',
        },
        function: {
          add: {
            method: 'post',
            url: () => ``,
          },
          edit: {
            method: 'put',
            url: ({ id }) => `/${id}`,
          },
          getList: {
            method: 'get',
            url: () => ``,
          },
          delete: {
            method: 'delete',
            url: ({ id }) => `/${id}`,
          },
          get: {
            method: 'get',
            url: ({ id }) => `/${id}`,
          },
          search: {
            method: 'get',
            url: () => `/search`,
          },
        },
      },
      'service-attribute': {
        config: {
          baseURL: '/service-attribute',
        },
        function: {
          add: {
            method: 'post',
            url: () => ``,
          },
          edit: {
            method: 'put',
            url: ({ id }) => `/${id}`,
          },
          getList: {
            method: 'get',
            url: () => ``,
          },
          delete: {
            method: 'delete',
            url: ({ id }) => `/${id}`,
          },
          get: {
            method: 'get',
            url: ({ id }) => `/${id}`,
          },
          search: {
            method: 'get',
            url: () => `/search`,
          },
        },
      },
      'service-category': {
        config: {
          baseURL: '/service-category',
        },
        function: {
          add: {
            method: 'post',
            url: () => ``,
          },
          edit: {
            method: 'put',
            url: ({ id }) => `/${id}`,
          },
          getList: {
            method: 'get',
            url: () => ``,
          },
          delete: {
            method: 'delete',
            url: ({ id }) => `/${id}`,
          },
          get: {
            method: 'get',
            url: ({ id }) => `/${id}`,
          },
          search: {
            method: 'get',
            url: () => `/search`,
          },
        },
      },
      'service-fee': {
        config: {
          baseURL: '/service-fee',
        },
        function: {
          add: {
            method: 'post',
            url: () => ``,
          },
          edit: {
            method: 'put',
            url: ({ id }) => `/${id}`,
          },
          getList: {
            method: 'get',
            url: () => ``,
          },
          delete: {
            method: 'delete',
            url: ({ id }) => `/${id}`,
          },
          get: {
            method: 'get',
            url: ({ id }) => `/${id}`,
          },
          search: {
            method: 'get',
            url: () => `/search`,
          },
        },
      },
      'step-price': {
        config: {
          baseURL: '/step-price',
        },
        function: {
          add: {
            method: 'post',
            url: () => ``,
          },
          edit: {
            method: 'put',
            url: ({ id }) => `/${id}`,
          },
          getList: {
            method: 'get',
            url: () => ``,
          },
          delete: {
            method: 'delete',
            url: ({ id }) => `/${id}`,
          },
          get: {
            method: 'get',
            url: ({ id }) => `/${id}`,
          },
          search: {
            method: 'get',
            url: () => `/search`,
          },
        },
      },
      addon: {
        config: {
          baseURL: '/addon',
        },
        function: {
          add: {
            method: 'post',
            url: () => ``,
          },
          edit: {
            method: 'put',
            url: ({ id }) => `/${id}`,
          },
          getList: {
            method: 'get',
            url: () => ``,
          },
          delete: {
            method: 'delete',
            url: ({ id }) => `/${id}`,
          },
          get: {
            method: 'get',
            url: ({ id }) => `/${id}`,
          },
          search: {
            method: 'get',
            url: () => `/search`,
          },
        },
      },
      attribute: {
        config: {
          baseURL: '/attribute',
        },
        function: {
          add: {
            method: 'post',
            url: '',
          },
          edit: {
            method: 'put',
            url: '',
          },
          getList: {
            method: 'get',
            url: '',
          },
          delete: {
            method: 'delete',
            url: '/:id',
          },
          get: {
            method: 'get',
            url: '/:id',
          },
          search: {
            method: 'get',
            url: '/search',
          },
        },
      },
      component: {
        config: {
          baseURL: '/component',
        },
        function: {
          add: {
            method: 'post',
            url: () => ``,
          },
          edit: {
            method: 'put',
            url: ({ id }) => `/${id}`,
          },
          getList: {
            method: 'get',
            url: () => ``,
          },
          delete: {
            method: 'delete',
            url: ({ id }) => `/${id}`,
          },
          get: {
            method: 'get',
            url: ({ id }) => `/${id}`,
          },
          search: {
            method: 'get',
            url: () => `/search`,
          },
        },
      },
      fee: {
        config: {
          baseURL: '/fee',
        },
        function: {
          add: {
            method: 'post',
            url: () => ``,
          },
          edit: {
            method: 'put',
            url: ({ id }) => `/${id}`,
          },
          getList: {
            method: 'get',
            url: () => ``,
          },
          delete: {
            method: 'delete',
            url: ({ id }) => `/${id}`,
          },
          get: {
            method: 'get',
            url: ({ id }) => `/${id}`,
          },
          search: {
            method: 'get',
            url: () => `/search`,
          },
        },
      },
      package: {
        config: {
          baseURL: '/package',
        },
        function: {
          add: {
            method: 'post',
            url: () => ``,
          },
          edit: {
            method: 'put',
            url: ({ id }) => `/${id}`,
          },
          getList: {
            method: 'get',
            url: () => ``,
          },
          delete: {
            method: 'delete',
            url: ({ id }) => `/${id}`,
          },
          get: {
            method: 'get',
            url: ({ id }) => `/${id}`,
          },
          search: {
            method: 'get',
            url: () => `/search`,
          },
        },
      },
      pricing: {
        config: {
          baseURL: '/pricing',
        },
        function: {
          add: {
            method: 'post',
            url: () => ``,
          },
          getStartDateAndEndDate: {
            method: 'post',
            url: () => `/get-start-date-and-end-date`,
          },
          getList: {
            method: 'get',
            url: () => ``,
          },
          delete: {
            method: 'delete',
            url: ({ id }) => `/${id}`,
          },
          get: {
            method: 'get',
            url: ({ id }) => `/${id}`,
          },
          search: {
            method: 'get',
            url: () => `/search`,
          },
        },
      },
      service: {
        config: {
          baseURL: '/service',
        },
        function: {
          add: {
            method: 'post',
            url: '',
          },
          edit: {
            method: 'put',
            url: ({ id }) => `/${id}`,
          },
          getList: {
            method: 'get',
            url: () => ``,
          },
          delete: {
            method: 'delete',
            url: ({ id }) => `/${id}`,
          },
          get: {
            method: 'get',
            url: ({ id }) => `/${id}`,
          },
          search: {
            method: 'get',
            url: () => `/search`,
          },
        },
      },
      unit: {
        config: {
          baseURL: '/unit',
        },
        function: {
          add: {
            method: 'post',
            url: () => ``,
          },
          edit: {
            method: 'put',
            url: ({ id }) => `/${id}`,
          },
          getList: {
            method: 'get',
            url: () => ``,
          },
          delete: {
            method: 'delete',
            url: ({ id }) => `/${id}`,
          },
          get: {
            method: 'get',
            url: ({ id }) => `/${id}`,
          },
          search: {
            method: 'get',
            url: () => `/search`,
          },
        },
      },
    },
  },
  'billing-service': {
    config: {
      baseURL: `${ROOT_API}/api/v2/billing-service`,
      timeout: 30000,
    },
    module: {
      invoices: {
        config: {
          baseURL: '/invoices',
        },
        function: {
          add: {
            method: 'post',
            url: () => ``,
          },
          edit: {
            method: 'put',
            url: ({ id }) => `/${id}`,
          },
          getList: {
            method: 'get',
            url: () => ``,
          },
          delete: {
            method: 'delete',
            url: ({ id }) => `/${id}`,
          },
          get: {
            method: 'get',
            url: ({ id }) => `/${id}`,
          },
          search: {
            method: 'get',
            url: () => `/search`,
          },
        },
      },
    },
  },
  'order-service': {
    config: {
      baseURL: `${ROOT_API}/api/v2/order-service`,
      timeout: 10000,
    },
    module: {
      order: {
        config: {
          baseURL: '/order',
        },
        function: {
          add: {
            method: 'post',
            url: () => ``,
          },
          edit: {
            method: 'put',
            url: ({ id }) => `/${id}`,
          },
          getList: {
            method: 'get',
            url: () => ``,
          },
          delete: {
            method: 'delete',
            url: ({ id }) => `/${id}`,
          },
          get: {
            method: 'get',
            url: ({ id }) => `/${id}`,
          },
          search: {
            method: 'get',
            url: () => `/search`,
          },
          updateInvoiceStatus: {
            method: 'post',
            url: () => `/update-invoice-status`,
          },
          updateSubcriberStatus: {
            method: 'post',
            url: () => `/update-subcriber-status`,
          },
        },
      },
      'order-package': {
        config: {
          baseURL: '/order-package',
        },
        function: {
          getList: {
            method: 'get',
            url: () => ``,
          },
          get: {
            method: 'get',
            url: ({ id }) => `/${id}`,
          },
        },
      },
      'order-package-extend': {
        config: {
          baseURL: '/order-package-extend',
        },
        function: {
          add: {
            method: 'post',
            url: () => ``,
          },
          edit: {
            method: 'put',
            url: ({ id }) => `/${id}`,
          },
          getList: {
            method: 'get',
            url: () => ``,
          },
          delete: {
            method: 'delete',
            url: ({ id }) => `/${id}`,
          },
          get: {
            method: 'get',
            url: ({ id }) => `/${id}`,
          },
          search: {
            method: 'get',
            url: () => `/search`,
          },
          updateInvoiceStatus: {
            method: 'post',
            url: () => `/update-invoice-status`,
          },
          updateSubcriberStatus: {
            method: 'post',
            url: () => `/update-subcriber-status`,
          },
        },
      },
    },
  },
  'subscription-service': {
    config: {
      baseURL: `${ROOT_API}/api/v2/subscription-service`,
      timeout: 10000,
    },
    module: {
      subcriber: {
        config: {
          baseURL: '/subcriber',
        },
        function: {
          add: {
            method: 'post',
            url: () => ``,
          },
          edit: {
            method: 'put',
            url: ({ id }) => `/${id}`,
          },
          getList: {
            method: 'get',
            url: () => ``,
          },
          delete: {
            method: 'delete',
            url: ({ id }) => `/${id}`,
          },
          get: {
            method: 'get',
            url: ({ id }) => `/${id}`,
          },
          search: {
            method: 'get',
            url: () => `/search`,
          },
          extend: {
            method: 'post',
            url: () => `/extend`,
          },
          'matbao.check-domain-price': {
            metod: 'get',
            url: () => '/matbao/check-domain-price',
          },
        },
      },
      'order-package': {
        config: {
          baseURL: '/order-package',
        },
        function: {
          getList: {
            method: 'get',
            url: () => ``,
          },
          get: {
            method: 'get',
            url: ({ id }) => `/${id}`,
          },
        },
      },
    },
  },
  'notification-service': {
    config: {
      baseURL: `https://api.copen.vn/notification`,
      timeout: 10000,
    },
    module: {
      'notification-email': {
        config: {
          baseURL: '/notification-email',
        },
        function: {
          add: {
            method: 'post',
            url: () => ``,
          },
        },
      },
    },
  },
};
