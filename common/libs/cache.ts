import { Logger } from '@nestjs/common';

const sentinelsHost = process.env.REDIS_SENTINEL_HOSTS.split(',') || [];
const sentinelsPort = Number(process.env.REDIS_SENTINEL_PORT);
const sentinels = sentinelsHost.map((item) => {
  return { host: item, port: sentinelsPort };
});
const TTL = Number(process.env.REDIS_TTL) || 60; //60s
const ENV = process.env.NODE_ENV || 'development';

const Redis = require('ioredis');
export const redisW = new Redis({
  sentinels: sentinels,
  name: `${process.env.REDIS_SENTINEL_NAME}`,
  sentinelPassword: `${process.env.REDIS_SENTINEL_PASS}`,
  password: `${process.env.REDIS_PASS}`,
  db: Number(`${process.env.REDIS_DB}`),
});

export const redisR = new Redis({
  sentinels: sentinels,
  name: `${process.env.REDIS_SENTINEL_NAME}`,
  role: 'slave',
  sentinelPassword: `${process.env.REDIS_SENTINEL_PASS}`,
  password: `${process.env.REDIS_PASS}`,
  db: Number(`${process.env.REDIS_DB}`),
});

redisW.on('error', function (err) {
  Logger.log('Error redisW ' + err);
});

redisW.on('connect', function () {
  Logger.log('connected to redisW ');
});

redisR.on('error', function (err) {
  Logger.log('Error redisR ' + err);
});

redisR.on('connect', function () {
  Logger.log('connected to redisR ');
});

export class Cache {
  async cacheFunctionBase(
    id,
    serviceName,
    moduleName,
    nameFunction,
    func,
    cache = true,
  ) {
    try {
      if (id) {
        const nameFunc = nameFunction;
        const key = `${
          ENV || 'dev'
        }:${serviceName}:${moduleName}:${nameFunc}:${id}`;
        let [dataCache, expire] = await Promise.all([
          redisR.get(key),
          redisR.ttl(key),
        ]);
        if (dataCache && cache === true) {
          if (TTL / (expire + 1) > 3) {
            exec(func);
          }
          return JSON.parse(dataCache);
        } else {
          return await exec(func);
        }

        async function exec(func) {
          const data = await func;
          if (data) {
            delete data.config;
            delete data.headers;
            delete data.request;
            delete data.status;
            delete data.statusText;

            await redisW.set(key, JSON.stringify(data));
            redisW.expire(key, TTL);
          }
          return data;
        }
      } else {
        return {};
      }
    } catch (error) {
      Logger.error(error);
      throw error;
    }
  }
}
