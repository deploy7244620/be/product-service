import {
  HttpException,
  HttpStatus,
  Injectable,
  NestMiddleware,
} from '@nestjs/common';
import { Request, Response, NextFunction } from 'express';
import jwt_decode from 'jwt-decode';
const msg401 = 'Unauthorized';
@Injectable()
export class AuthenMiddleware implements NestMiddleware {
  use(req: Request, res: Response, next: NextFunction) {
    const { authorization } = req.headers;
    if (!authorization)
      throw new HttpException(msg401, HttpStatus.UNAUTHORIZED);
    const [prefix, token] = authorization.split(' ');
    if (!['basic', 'bearer'].includes(prefix.toLowerCase()))
      throw new HttpException(msg401, HttpStatus.UNAUTHORIZED);
    if (authorization.toLowerCase().indexOf('basic ') === 0) {
      const base64String = authorization.split(' ')[1] || '';
      const [appId, appSecret] = Buffer.from(base64String, 'base64')
        .toString()
        .split(':');
      if (appSecret === process.env.PRIVATE_KEY) {
        return next();
      }
    }
    if (authorization.toLowerCase().indexOf('bearer ') === 0) {
      const [prefix, token] = authorization.split(' ');
      const realm = jwt_decode(token);
      if (!realm['user_id']) {
        throw new HttpException(msg401, HttpStatus.UNAUTHORIZED);
      }
      req.headers.userId = realm['user_id'];
      return next();
    }
    throw new HttpException(msg401, HttpStatus.UNAUTHORIZED);
  }
}
