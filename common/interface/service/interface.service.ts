export interface IService {
  _repo;

  add(input: any);
  update(id: any, data: any);
  delete(id: any);
  getById(id: any);
  select(input: any);
  search(input);
}
