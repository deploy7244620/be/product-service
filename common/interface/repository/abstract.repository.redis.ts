import { HttpException, HttpStatus } from '@nestjs/common';
const ENV = process.env.NODE_ENV || 'development';
import * as pack from '../../../package.json';

export abstract class AbstractRepositoryRedis {
  redisW: any;
  redisR: any;
  EntityRedis: any;
  constructor(private dataSource, private _EntityRedis) {
    this.redisW = dataSource.redisW;
    this.redisR = dataSource.redisR;
    this.EntityRedis = _EntityRedis;
  }
  async insert(input, module) {
    try {
      const promiseRedis = [];
      const entityRedis = new this.EntityRedis();
      Object.assign(entityRedis, input);

      entityRedis.defineKeyRedis.forEach((e) => {
        switch (e.configField.type) {
          case 'keyword': {
            if (e.nameScore) {
              promiseRedis.push([
                'zadd',
                `${ENV || 'dev'}:${pack.name}:${module}:${e.nameField}:${
                  e.nameScore || ''
                }:${entityRedis[e.nameField]}`,
                entityRedis[e.nameScore || ''] || 0,
                entityRedis['id'],
              ]);
            }
            promiseRedis.push([
              'zadd',
              `${ENV || 'dev'}:${pack.name}:${module}:${e.nameField}:${
                entityRedis[e.nameField]
              }`,
              new Date().getTime(),
              entityRedis['id'],
            ]);
            break;
          }
          case 'id': {
            promiseRedis.push(
              [
                'set',
                `${ENV || 'dev'}:${pack.name}:${module}:data:${
                  entityRedis['id']
                }`,
                JSON.stringify(input),
              ],
              [
                'zadd',
                `${ENV || 'dev'}:${pack.name}:${module}:id`,
                new Date().getTime(),
                `${entityRedis['id']}`,
              ],
            );
            break;
          }
          case 'number': {
            promiseRedis.push([
              'zadd',
              `${ENV || 'dev'}:${pack.name}:${module}:${e.nameField}`,
              entityRedis[e.nameField],
              entityRedis.id,
            ]);
            break;
          }

          default:
            break;
        }
      });
      await this.redisW.pipeline(promiseRedis).exec();
      return input;
    } catch (error) {
      throw new HttpException(error, HttpStatus.BAD_REQUEST);
    }
  }
  async update(id: string, input: any, module: any) {
    try {
      const promiseRedisDelete = [];
      const entityRedisOld = new this.EntityRedis();
      Object.assign(entityRedisOld, input);

      //get data redis
      let dataOld = await this.redisR.get(
        `${ENV || 'dev'}:${pack.name}:${module}:data:${id}`,
      );

      if (dataOld) {
        dataOld = JSON.parse(dataOld);
        const dataDelete = await this.delete(id, module);

        //set data
        Object.assign(dataOld, input);
        await this.insert(dataOld, module);
        const dataNew = await this.redisW.get(
          `${ENV || 'dev'}:${pack.name}:${module}:data:${id}`,
        );

        return JSON.parse(dataNew);
      } else {
        throw new HttpException('not found', HttpStatus.NOT_FOUND);
      }
    } catch (error) {
      throw new HttpException(error, HttpStatus.BAD_REQUEST);
    }
  }
  async delete(id: string, module: any): Promise<any> {
    try {
      let dataOld = await this.redisR.get(
        `${ENV || 'dev'}:${pack.name}:${module}:data:${id}`,
      );
      if (dataOld) {
        dataOld = JSON.parse(dataOld);
        const entityRedisOld = new this.EntityRedis();
        Object.assign(entityRedisOld, dataOld);
        const promiseRedisDelete = [];

        //delete update
        entityRedisOld.defineKeyRedis.forEach((e) => {
          switch (e.configField.type) {
            case 'keyword': {
              if (e.nameScore) {
                promiseRedisDelete.push([
                  'zrem',
                  `${ENV || 'dev'}:${pack.name}:${module}:${e.nameField}:${
                    e.nameScore || ''
                  }:${dataOld[e.nameField]}`,
                  dataOld['id'],
                ]);
              }
              promiseRedisDelete.push([
                'zrem',
                `${ENV || 'dev'}:${pack.name}:${module}:${e.nameField}:${
                  dataOld[e.nameField]
                }`,
                dataOld['id'],
              ]);

              break;
            }
            case 'id': {
              promiseRedisDelete.push(
                [
                  'del',
                  `${ENV || 'dev'}:${pack.name}:${module}:data:${
                    dataOld['id']
                  }`,
                ],
                [
                  'zrem',
                  `${ENV || 'dev'}:${pack.name}:${module}:id`,
                  `${dataOld['id']}`,
                ],
              );
              break;
            }
            case 'number': {
              promiseRedisDelete.push([
                'zrem',
                `${ENV || 'dev'}:${pack.name}:${module}:${e.nameField}`,
                `${dataOld['id']}`,
              ]);
              break;
            }
            default:
              break;
          }
        });
        const dataPromiseRedisDelete = await this.redisW
          .pipeline(promiseRedisDelete)
          .exec();

        return true;
      } else {
        return false;
      }
    } catch (error) {
      throw new HttpException(error, HttpStatus.BAD_REQUEST);
    }
  }
  async select(input: any, module) {
    try {
      // return await this.redisR
      //   .hscan(`${ENV || 'dev'}:${pack.name}:${module}:data`, 0, 'COUNT', 100)
      //   .then((rs) => {
      //     return { nextCount: rs[0], data: rs[1], allData: rs };
      //   });
      const page = input.page ? input.page : 1;
      const size = input.size || 10;

      const query = JSON.parse(JSON.stringify(input));
      delete query.page;
      delete query.size;

      if (query.id) {
        // tim kiem theo id
        const data = await this.redisR.get(
          `${ENV || 'dev'}:${pack.name}:${module}:data:${query.id}`,
        );
        if (data) {
          const output = {
            data: [JSON.parse(data)],
            count: 1,
            totalCount: 1,
            page: 1,
            size: 1,
            totalPage: 1,
          };
          return output;
        } else {
          const output = {
            data: [],
            count: 0,
          };
          return output;
        }
      } else {
        if (Object.keys(query).length == 0) {
          // get all
          // b1: get all key
          const allKey = await this.redisR.zrange(
            `${ENV || 'dev'}:${pack.name}:${module}:id`,
            Number((page - 1) * size || 0),
            Number((page - 1) * size || 0) + Number((size || 10) - 1),
          );
          const data = await this.redisR.mget(
            allKey.map(
              (x) => `${ENV || 'dev'}:${pack.name}:${module}:data:${x}`,
            ),
          );

          const output = {
            data: data.map((x) => JSON.parse(x || '')),
            count: (data || []).length,
            page,
            size,
          };

          return output;
        } else {
          // tim kiem nhieu dieu kien
          const arrayKeyword = [];
          const entityRedis = new this.EntityRedis();
          Object.assign(entityRedis, query);
          for (const key in query) {
            if (Object.prototype.hasOwnProperty.call(query, key)) {
              const element = query[key];
              arrayKeyword.push(
                `${ENV || 'dev'}:${pack.name}:${module}:${key}:${element}`,
              );
            }
          }

          const listIdZinter = await this.redisW
            .pipeline([['zinter', arrayKeyword.length].concat(arrayKeyword)])
            .exec();

          if (listIdZinter[0][1].length > 0) {
            const data = await this.redisR.mget(
              listIdZinter[0][1].map(
                (x) => `${ENV || 'dev'}:${pack.name}:${module}:data:${x}`,
              ),
            );

            const output = {
              data: data.map((x) => JSON.parse(x || '')),
              count: (data || []).length,
              page,
              size,
            };

            return output;
          } else {
            const output = {
              data: [],
              count: 0,
              totalCount: 0,
              page: 0,
              size: 0,
              totalPage: 0,
            };
            return output;
          }
        }
      }

      // return await this.dataSource.save(input);
    } catch (error) {
      throw new HttpException(error, HttpStatus.BAD_REQUEST);
    }
  }
  //   async findById(id: any) {
  //     try {
  //       return await this.dataSource.save(input);
  //     } catch (error) {
  //       throw new HttpException(error, HttpStatus.BAD_REQUEST);
  //     }
  //   }
  //   async findOne(option: any) {
  //     try {
  //       return await this.dataSource.save(input);
  //     } catch (error) {
  //       throw new HttpException(error, HttpStatus.BAD_REQUEST);
  //     }
  //   }
}
