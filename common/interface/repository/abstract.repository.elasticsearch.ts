import { IBaseRepositoryElasticsearch } from './interface.repository.elasticsearch';
import { client } from '../../libs/elasticsearch.connect';
import * as pack from '../../../package.json';
const NODE_ENV = process.env.NODE_ENV || 'DEV';
import { Logger } from '@nestjs/common';

export abstract class AbstractRepositoryElasticsearch
  implements IBaseRepositoryElasticsearch
{
  constructor(public dataSource, public nameIndex, public mapping) {
    nameIndex = buildNameIndex(nameIndex);
    this.createIndex(nameIndex, mapping);
  }
  async createIndex(nameIndex, mapping) {
    const data = await client.indices.exists({
      index: nameIndex,
    });
    if (data.body) {
      // đã tồn tại index
      Logger.log('đã tồn tại index ', nameIndex);
      return;
    } else {
      // chưa tồn tại index
      return await this.dataSource.indices.create({
        index: nameIndex,
        body: {
          mappings: mapping,
        },
      });
    }
  }

  async createDoc(nameIndex, doc) {
    nameIndex = buildNameIndex(nameIndex);
    delete doc.createBy;
    delete doc.updateBy;
    return await this.dataSource.index({
      index: nameIndex,
      id: doc.id,
      body: doc,
    });
  }

  async updateDoc(nameIndex, idDoc, doc) {
    try {
      nameIndex = buildNameIndex(nameIndex);
      delete doc.createBy;
      delete doc.updateBy;
      return await this.dataSource.update({
        index: nameIndex,
        id: idDoc,
        body: {
          doc: doc,
        },
      });
    } catch (error) {
      throw error;
    }
  }

  async deleteDoc(nameIndex, idDoc) {
    nameIndex = buildNameIndex(nameIndex);
    return await this.dataSource.delete({
      index: nameIndex,
      id: idDoc,
    });
  }

  async search(nameIndex, query) {
    nameIndex = buildNameIndex(nameIndex);
    const _query = {
      query: {
        bool: {
          must: [],
        },
      },
    };
    const size = Number(query.size || 20);
    const page = Number(query.page ? query.page : 1) - 1;
    const from = page * size;
    delete query.size;
    delete query.page;

    for (const key in query) {
      if (Object.prototype.hasOwnProperty.call(query, key)) {
        if (typeof query[key] == 'object') {
          if (
            query[key].gte ||
            query[key].gt ||
            query[key].lte ||
            query[key].lt
          ) {
            const range = {};
            range[key] = query[key];
            _query.query.bool.must.push({
              range,
            });
          }
        } else {
          const element = query[key];
          const match = {};
          match[key] = element;
          _query.query.bool.must.push({
            match,
          });
        }
      }
    }

    const data = await this.dataSource.search({
      index: nameIndex,
      body: _query,
      size,
      from,
    });
    const hits = data.body.hits.hits;
    const output = new OutputSelect(hits.map((rs) => rs._source));
    output.query = _query;
    output.count = hits.length;
    output.totalCount = data.body.hits.total.value;
    output.page = page + 1;
    output.size = size;
    output.totalPage =
      output.totalCount % output.count === 0
        ? output.totalCount / output.count
        : Math.floor((output.totalCount + output.size - 1) / output.size || 1);
    return output;
  }

  async deleteIndex(nameIndex) {
    nameIndex = buildNameIndex(nameIndex);
    return await this.dataSource.indices.delete({ index: nameIndex });
  }

  async syncDataSearch() {}
}

export class OutputSelect {
  constructor(public data: []) {}
  page: number;
  size: number;
  totalPage: number;
  totalCount: number;
  count: number;
  query: any;
}

export function buildNameIndex(nameIndex) {
  return `${NODE_ENV}_${pack.name}_${nameIndex}`.toLowerCase();
}
