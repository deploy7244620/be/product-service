export interface BaseGetListDto {
  page: number;
  size: number;
  orderBy: object;
  updateAtTimestamp: object;
}
