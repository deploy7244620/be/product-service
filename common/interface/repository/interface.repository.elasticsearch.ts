export class OutputSelect<Entity> {
  constructor(public data: Entity[]) {}
  page: number;
  size: number;
  totalPage: number;
  totalCount: number;
  count: number;
}
export interface IBaseRepositoryElasticsearch {
  createIndex(nameIndex, mapping): Promise<any>;
  createDoc(nameIndex, doc): Promise<any>;
  updateDoc(nameIndex, idDoc, doc): Promise<any>;
  deleteDoc(nameIndex, idDoc): Promise<any>;
  search(nameIndex, query): Promise<any>;
  deleteIndex(nameIndex): Promise<any>;
  syncDataSearch(): Promise<any>;
}
