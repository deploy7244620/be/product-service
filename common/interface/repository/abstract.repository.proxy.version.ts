import { IBaseRepository, OutputSelect } from './interface.repository';

export abstract class AProxy<Entity> implements IBaseRepository<Entity> {
  findOne(option: any): Promise<Entity> {
    throw new Error('Method not implemented.');
  }
  insert(input: Entity): Promise<Entity> {
    throw new Error('Method not implemented.');
  }
  update(id: string, data: any): Promise<Entity> {
    throw new Error('Method not implemented.');
  }
  delete(input: string): Promise<any> {
    throw new Error('Method not implemented.');
  }
  select(input: any): Promise<OutputSelect<Entity>> {
    throw new Error('Method not implemented.');
  }
  findById(id: any): Promise<Entity> {
    throw new Error('Method not implemented.');
  }
}
