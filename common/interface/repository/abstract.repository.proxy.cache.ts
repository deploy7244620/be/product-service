/*
class proxy phuc vu ưu tiên việc lưu dữ liệu vào cache 
RedisId: có TTL
- lưu cục dữ liệu của object
- lưu dữ liệu là string thường hoặc hash. 

RedisKeyword: không TTL
- đánh dấu dữ liệu trường này là keyword. chỉ tìm kiếm bằng. không đấu
- lưu dữ liệu kiểu list
- mặc định không có score
- đẩy các trường number vào để query thêm ngày tháng

RedisNumber: không TTL
- lưu trữ dữ liệu là number để query từ ngày đến ngày
- lưu dữ liệu kiểu zset
- lưu score = giá trị và value = id

*/
import { HttpException, HttpStatus } from '@nestjs/common';
import { MessageBus } from 'common/libs/message-bus';
import { IBaseRepository, OutputSelect } from './interface.repository';
const messageBus = new MessageBus();
const EXCHANGE_DEFAULT = process.env.EXCHANGE_DEFAULT || 'EXCHANGE_DEFAULT';
const NAME_QUEUE = {
  INSERT: {
    nameQueue: 'INSERT',
    nameWorker: 'insertDataElasticsearch',
  },
  UPDATE: {
    nameQueue: 'UPDATE',
    nameWorker: 'updateDataElasticsearch',
  },
  DELETE: {
    nameQueue: 'DELETE',
    nameWorker: 'deleteDataElasticsearch',
  },
};

export abstract class AProxy {
  constructor(
    public nameModule,
    public storage,
    public storageSearch?,
    public storageCache?,
  ) {
    if (!storageCache) {
      throw 'storageCache not null';
    }

    if (this.storageSearch) {
      for (const key in NAME_QUEUE) {
        if (Object.prototype.hasOwnProperty.call(NAME_QUEUE, key)) {
          const element = NAME_QUEUE[key];
          messageBus.run(
            this,
            element.nameWorker,
            messageBus.buildNameQueue(element.nameQueue, this.nameModule),
          );
        }
      }
    }
  }
  private async insertDataElasticsearch(input) {
    return await this.storageSearch.createDoc(
      this.storageSearch.nameIndex,
      input,
    );
  }

  private async updateDataElasticsearch(input) {
    return await this.storageSearch.updateDoc(
      this.storageSearch.nameIndex,
      input.id,
      input.data,
    );
  }

  private async deleteDataElasticsearch(input) {
    return await this.storageSearch.deleteDoc(
      this.storageSearch.nameIndex,
      input,
    );
  }

  async findOne(option: any) {
    try {
      return await this.storage.findOne(option);
    } catch (error) {
      throw new HttpException(error, HttpStatus.BAD_GATEWAY);
    }
  }

  async insert(input) {
    try {
      // const dataPostgres = await this.storage.insert(input);
      const data = await this.storageCache.insert(input, this.nameModule);
      if (this.storageSearch && this.storageSearch.nameIndex) {
        await messageBus.sendToQueue(
          data,
          EXCHANGE_DEFAULT,
          messageBus.buildNameQueue(
            NAME_QUEUE.INSERT.nameQueue,
            this.nameModule,
          ),
        );
      }
      return data;
    } catch (error) {
      throw new HttpException(error, HttpStatus.BAD_GATEWAY);
    }
  }
  async update(id: string, data: any) {
    try {
      const rs = await this.storageCache.update(id, data, this.nameModule);
      if (this.storageSearch && this.storageSearch.nameIndex) {
        await messageBus.sendToQueue(
          { id, data },
          EXCHANGE_DEFAULT,
          messageBus.buildNameQueue(
            NAME_QUEUE.UPDATE.nameQueue,
            this.nameModule,
          ),
        );
      }
      return rs;
    } catch (error) {
      throw new HttpException(error, HttpStatus.BAD_GATEWAY);
    }
  }
  async delete(input: string): Promise<any> {
    try {
      // const rs = await this.storage.delete(input);
      const rs = await this.storageCache.delete(input, this.nameModule);
      if (this.storageSearch && this.storageSearch.nameIndex) {
        await messageBus.sendToQueue(
          input,
          EXCHANGE_DEFAULT,
          messageBus.buildNameQueue(
            NAME_QUEUE.DELETE.nameQueue,
            this.nameModule,
          ),
        );
      }
      return rs;
    } catch (error) {
      throw new HttpException(error, HttpStatus.BAD_GATEWAY);
    }
  }
  async select(input: any) {
    try {
      //const rs = await this.storage.select(input);
      const data = await this.storageCache.select(input, this.nameModule);
      return data;
    } catch (error) {
      throw new HttpException(error, HttpStatus.BAD_GATEWAY);
    }
  }
  async findById(id: any) {
    try {
      return await this.storage.findById(id);
    } catch (error) {
      throw new HttpException(error, HttpStatus.BAD_GATEWAY);
    }
  }

  async search(input) {
    try {
      return await this.storageSearch.search(
        this.storageSearch.nameIndex,
        input,
      );
    } catch (error) {
      throw new HttpException(error, HttpStatus.BAD_GATEWAY);
    }
  }
}
