export interface IBaseEntity {
  note: string; // chú ý. hiển thị nội bộ, không hiển thị cho người dùng xem
  description: string; // hiển thị cho người dùng
  status: boolean; // trạng thái dữ liệu status = false -> đã xóa
  isActive: boolean; // trạng thái hiển thị ngoài giao diện isActive = false -> không hiển thị trên web
  updateAt: Date; // thời gian sửa dữ liệu theo định dạng date
  updateAtTimestamp: number; // thời gian sửa dữ liệu theo định dạng timestamp
  updateBy: string; // người sửa dữ liệu gần nhất
  createAt: Date;
  createAtTimestamp: number;
  createBy: string;
}
