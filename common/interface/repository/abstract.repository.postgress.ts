import { HttpException, HttpStatus } from '@nestjs/common';
import {
  Between,
  LessThan,
  LessThanOrEqual,
  ILike,
  MoreThan,
  MoreThanOrEqual,
} from 'typeorm';
import { IBaseRepository, OutputSelect } from './interface.repository';

export abstract class AbstractRepositoryPostgres<T>
  implements IBaseRepository<T>
{
  constructor(private dataSource) {}
  async findOne(option: any): Promise<T> {
    try {
      return await this.dataSource.findOneBy(option);
    } catch (error) {
      throw new HttpException(error, HttpStatus.BAD_REQUEST);
    }
  }

  async insert(input: T): Promise<T> {
    try {
      return await this.dataSource.save(input);
    } catch (error) {
      throw new HttpException(error, HttpStatus.BAD_REQUEST);
    }
  }
  async update(id: string, input: any): Promise<T> {
    try {
      // const data = await this.dataSource.findOne({ where: { id } });
      const data = await this.dataSource.update(id, input);
      if (data.affected == 1) {
        return await this.dataSource.findOne({ where: { id } });
      } else {
        throw new HttpException('ID KHONG TON TAI', HttpStatus.NOT_FOUND);
      }
    } catch (error) {
      throw new HttpException(error, HttpStatus.BAD_REQUEST);
    }
  }
  async delete(id: string): Promise<any> {
    try {
      // return await this.dataSource.delete(input);
      const data = await this.select({ id, status: true });
      if (data.count > 0) {
        await this.dataSource.update(id, { status: false });
        return true;
      } else {
        return false;
      }
    } catch (error) {
      throw new HttpException(error, HttpStatus.BAD_REQUEST);
    }
  }
  async select(input: any): Promise<OutputSelect<T>> {
    try {
      const where = {};
      const page = input.page ? input.page : 1;
      const size = input.size || 100;
      const orderBy = input.orderBy;

      Object.keys(input).forEach((element) => {
        switch (typeof input[element]) {
          case 'object':
            {
              if (
                input[element].gte ||
                input[element].gt ||
                input[element].lte ||
                input[element].lt
              ) {
                if (
                  (input[element].gte || input[element].gt) &&
                  (input[element].lte || input[element].lt)
                ) {
                  where[element] = Between(
                    input[element].gte || input[element].gt,
                    input[element].lte || input[element].lt,
                  );
                } else {
                  if (input[element].gte) {
                    // hớn hơn hoặc bằng
                    where[element] = MoreThanOrEqual(input[element].gte);
                  }
                  if (input[element].lte) {
                    // nhỏ hơn hoặc bằng
                    where[element] = LessThanOrEqual(input[element].lte);
                  }
                  if (input[element].gt) {
                    // lớn hơn
                    where[element] = MoreThan(input[element].gt);
                  }
                  if (input[element].lt) {
                    // nhỏ hơn
                    where[element] = LessThan(input[element].lt);
                  }
                }
              } else {
                where[element] = input[element];
              }
            }
            break;
          default:
            {
              where[element] = input[element];
            }
            break;
        }
      });
      where['status'] = true;
      delete where['page'];
      delete where['size'];
      delete where['orderBy'];

      const data = await this.dataSource.findAndCount({
        where,
        skip: Number((page - 1) * size || 0),
        take: Number(size || 100),
        order: orderBy || { createAtTimestamp: 'DESC' },
      });

      const output = new OutputSelect<T>(data[0]);
      output.count = data[0].length;
      output.totalCount = data[1];
      output.page = page;
      output.size = size;
      output.totalPage =
        output.totalCount % output.count === 0
          ? output.totalCount / output.count
          : Math.floor(
              (output.totalCount + output.size - 1) / output.size || 1,
            );

      return output;
    } catch (error) {
      throw new HttpException(error, HttpStatus.BAD_REQUEST);
    }
  }
  async findById(id: any): Promise<T> {
    try {
      return await this.dataSource.findOneBy({ id });
    } catch (err) {
      throw new HttpException(err, HttpStatus.BAD_REQUEST);
    }
  }
}
